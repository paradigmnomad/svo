# Seasonal Villager Outfits

[Neuxs Page](https://www.nexusmods.com/stardewvalley/mods/2449/)

# Current Config Options
`*` Options include an `AllowBlank: true` option.

## Whole File
These options affect the mod in general.

option                         | valid values (select only one, values are seperated by a space)
----------------------------   | ------- 
`EnableOptimization`           | true false
`PortraitStyle`*               | standard
`MaterintySprites`             | true false
`SwimSuitsEnabled`             | true false

## Standard Portraits
These options only apply when you set `PortraitStyle` to `standard`.

options                        | valid values (select only one, values are seperated by a space)
----------------------------   | ------- 
`ElliottPortraitStyle`         | standard frontfacingeemie frontfacinglumen
`HarveyFacialHair`             | mustache shaved
`HarveyShavedVariant`          | round slim
`SamSummerPortraitVariant`     | standard alternative
`SandyPortraitVariant`         | side sivs front
`WillyWinterPortraitVariant`   | standard alternative

## DCBurger Portraits
These options only apply when you set `PortraitStyle` to `dcburger`.

options                        | valid values (select only one, values are seperated by a space)
----------------------------   | ------- 
`DC_AlexVersion`               | standard darkerjacket
`DC_ClintVersion`              | standard snow snowfog
`DC_EmilySpringVersion`        | standard alternative
`DC_EmilyFallyVersion`         | standard alternative
`DC_LewisVersion`              | tie notie
`DC_HaleyFallRainVersion`      | standard alternative
`DC_HarveyVersion`             | standard nobeard nobeardglasses noglasses
`DC_HarveySummerShirt`         | whitenotie bluenotie whitetie bluetie
`DC_KentVersion`               | standard outline alternative
`DC_MaruVersion`               | noglasses glasses
`DC_PierreVersion`             | standard fog
`DC_SebastianVersion`          | standard choker fog chokerfog

## Characters
These options affect the character files.

options                        | valid values (select only one, values are seperated by a space)
----------------------------   | ------- 
`AbigailSummerRainVariant`     | nohat hat
`AbigailEggFestivalVariant`    | standard alternative
`AlexSummerVariant`            | sunglasses nosunglasses
`ClintVariant`                 | apron noapron
`ElliottSummerVariant`         | nosuspenders suspenders vest novest
`EmilyWinterVariant`           | hatnoscarf hatscarf nohatnoscarf nohatscarf
`EmilySpringVariant`           | sleeves nosleeves
`EmilySpringRainVariant`       | pink yellow
`EmilyEggFestivalVariant`      | standard alternative
`EmilyEggFestivalVariantColor` | blue red
`EvelynWinterColor`            | pink blue
`HaleySummerVariant`           | indoor beach
`HaleyFallColor`               | red blue
`HaleyWinterColor`             | grey pink
`HaleyEggFestivalVariant`      | standard alternative
`HaleyEggFestivalVariantColor` | blue pink
`HarveySpringVariant`          | vest novest
`HarveyFallRainVariant`        | blackredscarf brownnoscarf
`LeahWinterColorVariant`       | red blue
`LeahWinterHood`               | up down
`LeahEggFestivalVariant`       | standard alternative
`MaruScrubColor`               | dark light white
`MaruEggFestivalVariant`       | standard alternative
`PennyHair`                    | standard brunette
`PennyEggFestivalVariant`      | standard alternative
`RobinHairStyle`               | up down
`SamSpiritsEve`                | madscientist scarecrow
`SamScarecrowFacepaint`        | facepaint nofacepaint
`SandyEnabled`                 | true false
`SandyVersion`                 | standard sivs zola
`SebastianSpiritsEve`          | mask nomask
`WizardEnabled`                | true false
`WizardVersion`                | standard pale saturated


## Swimsuits
These options affect the swimsuit options in Summer on the beach (if enabled).

options                        | valid values (select only one, values are seperated by a space)
----------------------------   | ------- 
`ElliottSwimVariant`           | paunch thin
`GeorgeSwimVariant`            | default floaties
`HaleySwimVariant`             | white pink
`LinusSwimVariant`             | censored grassskirt
`PennySwimVariant`             | blue yellow
